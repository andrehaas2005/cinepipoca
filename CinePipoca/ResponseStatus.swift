//
//  ResponseStatus.swift
//  CinePipoca
//
//  Created by André Haas on 19/05/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct ResponseStatus {
    let status_code : Int?
    let status_message : String?
}
