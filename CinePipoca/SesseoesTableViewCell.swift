//
//  SesseoesTableViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 05/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class SesseoesTableViewCell: UITableViewCell {

    @IBOutlet weak var imPoster: UIImageView!
    @IBOutlet weak var lbEpisodio: UILabel!
    @IBOutlet weak var lbResumo: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbNumerEpisodio: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func prepareCell(_ episodio: Episodio){
        imPoster.image = #imageLiteral(resourceName: "semImages")
        lbEpisodio.text = episodio.name
        lbResumo.text = episodio.overview
        lbDate.text = episodio.air_date
        lbNumerEpisodio.text = String(episodio.episode_number)
    }
    
}
