//
//  SeasonModel.swift
//  CinePipoca
//
//  Created by André Haas on 29/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct Season : Decodable {
    let _id : String
    let air_date : String
    let episodes : [Episodio]
    let name : String
    let overview : String?
    let id : Int
    let poster_path : String?
    let season_number : Int
    
}
struct Episodio : Decodable {
    let air_date : String
    let episode_number : Int
    let name : String
    let overview : String?
    let id : Int
    let production_code : String?
    let season_number : Int
    let still_path :String?
    let vote_average : Double
    let vote_count : Int
}
