//
//  SessionModel.swift
//  CinePipoca
//
//  Created by André Haas on 18/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct SessionModel : Decodable {
    var session_id : String!
    var  success : Bool!
}
