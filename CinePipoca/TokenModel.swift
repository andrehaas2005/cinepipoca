//
//  TokenModel.swift
//  CinePipoca
//
//  Created by André Haas on 18/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct TokenModel : Decodable  {
    
    var expires_at : String!
    var request_token : String!
    var seccess : Bool!
    
    /*
     {
     "expires_at" = "2018-03-18 23:12:02 UTC";
     "request_token" = 3e99648bc2df671d4a19f755e639edaaa17501f6;
     success = 1;
     }
     */
    
}
