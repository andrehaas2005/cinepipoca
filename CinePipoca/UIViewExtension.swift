//
//  UIViewExtension.swift
//  CinePipoca
//
//  Created by André Haas on 30/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundViewWithBorderWidth( borderWidth: CGFloat, andBorderColor color: UIColor, andCornerRadius cornerRadius: CGFloat){
        
        let viewLayer = self.layer
        
        viewLayer.masksToBounds = true
        viewLayer.borderWidth = borderWidth
        viewLayer.borderColor = color.cgColor
        viewLayer.cornerRadius = cornerRadius
        
    }
    
    func roundViewWithBorderWidth(borderWidth: CGFloat, andBorderColor color: UIColor){
        
        let viewLayer = self.layer
        
        viewLayer.masksToBounds = true
        viewLayer.borderWidth = borderWidth
        viewLayer.borderColor = color.cgColor
        
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 6000.0, completionDelegate: AnyObject? = nil) {
        
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 2000.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

extension UIView {
    func rotate(toValue: CGFloat, duration: CFTimeInterval = 0.2, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = toValue
        rotateAnimation.duration = duration
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.fillMode = kCAFillModeForwards
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
