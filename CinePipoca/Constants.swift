//
//  Constants.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
//
//enum Constants : String  {
//    case chaveAcesso = "8fb08dee4651c3d9b1536a69ea3f3d16"
//    case urlBase = "https://api.themoviedb.org/3/"
//    case urlImage = "http://image.tmdb.org/t/p/w500/"
//    case idConta = "6198042"
//
//}

struct Constants {
    static let menu = [""]
    static let icons = [""]
    static let chaveAcesso = "8fb08dee4651c3d9b1536a69ea3f3d16"
    static let urlBase = "https://api.themoviedb.org/3/"
    static let urlImage = "http://image.tmdb.org/t/p/w500/"
    static let idConta = "6198042"
    static let urlYoutube = "https://www.youtube.com/watch?v="
    
    struct Mensagem {
        static let SemInternet = "Vefifique sua conexão com a internet e tente novament. Obrigado!"
        static let ErroNaoMapeado = "Ocorreu um erro não identificado, favor tentar novamente mais tarde. Obrigado!"
    }
    
    struct Key {
        static let User = "User"
        static let AccountId = "account_id"
        static let SessionID = "SessionID"
    }
}
