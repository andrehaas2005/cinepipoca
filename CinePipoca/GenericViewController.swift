//
//  GenericViewController.swift
//  CinePipoca
//
//  Created by André Haas on 19/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

protocol menuProtocol {
    func menuClose()
}

class GenericViewController: UIViewController, menuProtocol {
    
    
    //Color
    var colorBackGroud : UIColor!// teste?.background
    var colorPrimary : UIColor! // teste?.primary
    var colorSecondary : UIColor! // teste?.secondary
    var colorDetail : UIColor! // teste?.detail
    
    var menu_vc : MenuViewController!
    var customLoadView : CustomLoadView!
    var menuControle = false
    var procotol = menuProtocol?.self
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chamaMenu()
        self.customLoadView = CustomLoadView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func chamaMenu(){
        menu_vc = self.storyboard?.instantiateViewController(withIdentifier: "Menu") as! MenuViewController
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.responseToGesture))
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.responseToGesture))
        swipeLeft.direction = .left
        swipeRight.direction = .right
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
    }
    
    
    func getCores(_ image : UIImage) {
        let cores = image.getColors()
        
        self.colorBackGroud = cores.background
        self.colorPrimary = cores.primary
        self.colorSecondary = cores.secondary
        self.colorDetail = cores.detail
    }
    
    func showHud(){
        self.customLoadView.displayView()
        
    }
    
    func hideHud(){
        customLoadView.hideView()
    }
    
    // Menu Lateral
    @objc func responseToGesture(gesture: UISwipeGestureRecognizer){
        
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            menuOpen()
            menuControle = true
        case UISwipeGestureRecognizerDirection.left:
            menuClose()
            menuControle = false
        default:
            break
        }
        
    }
    
    
    func menuOpen(){
        
        print("Open em Geberic")
        UIView.animate(withDuration: 0.3) {
            UIApplication.shared.keyWindow?.addSubview(self.menu_vc.view)
        }
        
    }
    
    func menuClose(){
        print("Close em Geberic")
        UIView.animate(withDuration: 0.3) {
            UIApplication.shared.keyWindow?.willRemoveSubview(self.menu_vc.view)
        }
        
    }
    
    
    func alertGeneric(typeResponse : TypeResponse, view :  UIViewController){
        var msg = ""
        var title = ""
        switch typeResponse {
            
        case .Sucesso(let sucess):
            if let mensagem = sucess as? String {
                msg = mensagem
                title = "Sucesso !!!"
            }
        case .SemInternet:
            msg = Constants.Mensagem.SemInternet
            title = "Ops !!! Erro..."
        case .Error(let error):
            if let err = error as? Error {
                msg = err.localizedDescription
                title = "Ops !!! Erro..."
            }else{
                msg = Constants.Mensagem.ErroNaoMapeado
                title = "Ops !!! Erro..."
            }
        }
        
        let alertControll = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertControll.addAction(action)
        view.present(alertControll, animated: true, completion: nil)
        
    }
    
}
