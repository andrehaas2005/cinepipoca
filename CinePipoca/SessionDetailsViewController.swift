//
//  SessionDetailsViewController.swift
//  CinePipoca
//
//  Created by André Haas on 05/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import Kingfisher
import UserNotifications
import Auk
import FirebaseDatabase


class SessionDetailsViewController: GenericViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var seasonNumber: Int!
    var serieId: Int!
    var season: Season?
    let  urlImage =  Constants.urlImage
    var nomeSerie: String!
    var datePicker: UIDatePicker!
    var dateSelected: String!
    var timeSerie: String!
    var imagens: [UIImage]!
    var seriesManager = SeriesManager.share
    var userInfo : UserInfo!
    var episodioAssistidos : [String : Any] = [:]
    
    /*
     
     let dateBase = Database.database().reference()
     let usuarios = dateBase.child("Usuarios")
     let usuario = usuarios.child("\(userInfo.id!)").child(userInfo.name)
     let serie = usuario.child("\(serieId!)")
     */
   var serie: DatabaseReference!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showHud()
        getUser()
        serie = Database.database().reference().child("Usuarios").child("\(self.userInfo.id!)").child(self.userInfo.name).child("\(serieId!)")
        serie.observe(DataEventType.value) { (dados) in
         let postDict = dados.value as? [String : AnyObject] ?? [:]
            if let episo = postDict["Temporada \(self.seasonNumber!)"] as? [String : Any]{
            self.episodioAssistidos = episo
            }
           self.loadEpisodios()
        }
       
        loadImage()
       // Do any additional setup after loading the view.
    }
    
    
    func getUser(){
        if let userData = UserDefaults.standard.object(forKey: "User") as? Data {
            let user = try! JSONDecoder().decode(UserInfo.self, from: userData)
            self.userInfo = user
            
        }
    }
    
    func loadImage(){
        seriesManager.getImagensSerie(serie_id: serieId) { (retorno) in
            if retorno != nil {
                for backdrop in (retorno?.backdrops)!{
                    print(backdrop.file_path)
                    
                    self.scrollView.auk.show(url: "\(Constants.urlImage)\(backdrop.file_path)")
                    self.scrollView.auk.startAutoScroll(delaySeconds: 3.0)
                    
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func selectDate(_ sender: UIDatePicker){
        
       self.timeSerie = DateHelper.FormatarDataToStringHora(date: sender.date)
        print(self.dateSelected)
        
    }
    
    private func agendar(_ episodio: Episodio){
        
        //id da notificacao unico
        let id = String(Date().timeIntervalSince1970)
        
       
        let content = UNMutableNotificationContent()
        content.title = "Lembrete : \((self.season!.name))"
        content.subtitle = "\(self.nomeSerie!)"
        
        //let data = String(format: "dd/MM/yyyy", arguments: [episodio.air_date])
        
        
        content.body = "Sua Serie favorita '\(self.nomeSerie!)' será exibido o episório : \(episodio.name) no dia (\(DateHelper.FormatarStringData(dataString: episodio.air_date) ))\n Lembrar no dia: \(DateHelper.ReturnaMaiorData(dataString: episodio.air_date))"
        
        content.categoryIdentifier = "Lembrete"
        // essa trigger dispara depois que ela é criada no intervalo de 15 segundos, conforme o valor setado no
        // timeInterval
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        let dataDeAgendamento = DateHelper.ReturnaMaiorData(dataString: episodio.air_date)
        let agenda = "\(dataDeAgendamento) \(self.timeSerie!)"
        
        let dataAgendada = DateHelper.FormatarStringToData(dataString: agenda)
        
        
        //Trigger agendada
        let dateComponets = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: dataAgendada)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponets, repeats: false)
        
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    
    func opcaoSerie(_ episodio : Episodio){
        let alertController = UIAlertController(title: nil, message: "O que pretende marcar nesse episodio S\(episodio.season_number)E\(episodio.episode_number)", preferredStyle: .actionSheet)
        
        let agendarAction = UIAlertAction(title: "Agendar", style: .default) { (action) in
            self.agendarAlerta(episodio)
        }
        
        let jaAssistido = UIAlertAction(title: "Episodio Assisitido", style: .default) { (action) in
            self.marcaAssistido(episodio)
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alertController.addAction(jaAssistido)
        alertController.addAction(agendarAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func marcaAssistido(_ episodio : Episodio){
//        let dateBase = Database.database().reference()
//        let usuarios = dateBase.child("Usuarios")
//        let usuario = usuarios.child("\(userInfo.id!)").child(userInfo.name)
//       let serie = usuario.child("\(serieId!)")
        serie.child("Nome Serie").setValue(nomeSerie)
        let temporada = serie.child("Temporada \(episodio.season_number)")
        let episodioRef = temporada.child("Episodio \(episodio.episode_number)")
        episodioRef.child("Nome").setValue(episodio.name)
        episodioRef.child("DataExibicao").setValue(episodio.air_date)
        episodioRef.child("Overview").setValue(episodio.overview)
        episodioRef.child("EpisodioId").setValue(episodio.id)
        
       
    }
    
    private func agendarAlerta(_ episodio : Episodio){
       self.dateSelected = DateHelper.FormatarStringData(dataString: episodio.air_date)
        let alertController = UIAlertController(title: "Agendar notificação ? ", message: "\(season!.name)\n Episodio: \(episodio.name)\n Dia: \(self.dateSelected!)", preferredStyle: .alert)
        
        let agendarAction = UIAlertAction(title: "Agendar", style: .default) { (action) in
            self.agendar(episodio)
        }
        let cancelarAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        
        
        self.datePicker = UIDatePicker(frame: CGRect(x: 85, y: 85, width: 100, height: 180))
        self.datePicker.datePickerMode = .time
        self.datePicker.setDate(Date(), animated: true)
        self.datePicker.timeZone = TimeZone(abbreviation: "UTC")
        self.datePicker.locale = Locale(identifier: "pt_BR")
        
        self.datePicker.addTarget(self, action: #selector(selectDate(_:)), for: .valueChanged)
        
        
        alertController.addAction(agendarAction)
        alertController.addAction(cancelarAction)
        
        let height : NSLayoutConstraint = NSLayoutConstraint(item: alertController.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 300)
        alertController.view.addConstraint(height)
        
        
        alertController.view.addSubview(self.datePicker)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    func loadEpisodios(){
        
        seriesManager.getEpisodio(serie_id: serieId, season: seasonNumber, onComplete: { (retorno) in
            self.season = retorno
            if self.season != nil {
                self.tableView.dataSource = self
                self.tableView.delegate = self
                self.tableView.reloadData()
                self.hideHud()
            }
        })
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueEpisodioDetalhes"{
            if let vc = segue.destination as? EpisodioDetalhesViewController {
                if let episodio = sender as? Episodio {
                    vc.ep = episodio
                    vc.jaAssistido = self.assistido(ep: episodio)
                }
            }
        }
    }
    
}

extension SessionDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.season?.episodes.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! EpisodioTableViewCell
        let ep = self.season?.episodes[indexPath.row]
        let isAssistido = assistido(ep: ep!)
        cell.preparaCell(ep: ep!, assistido: isAssistido)
        cell.contentView.backgroundColor = self.colorBackGroud
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
       
        cell.contentView.addGestureRecognizer(longPressRecognizer)
        
        return cell
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        
        if sender.state == UIGestureRecognizerState.began {
            let touchPoint = sender.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                print("Long pressed row: \(indexPath.row)")
                let episodio = season?.episodes[indexPath.row]
                self.opcaoSerie(episodio!)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ep = self.season?.episodes[indexPath.row]
        
        performSegue(withIdentifier: "segueEpisodioDetalhes", sender: ep)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SessionDetailsViewController {
    
    func assistido(ep: Episodio)-> Bool {
        if let episo =  self.episodioAssistidos["Episodio \((ep.episode_number))"] as? [String : Any]{
            if let episID =  episo["EpisodioId"] as? Int {
                if episID == (ep.id) {
                   return true
                    
                }else{
                    return false
                }
            }
        }
        return false
    }
    
    
}
