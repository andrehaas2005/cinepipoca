//
//  EpisodioDetalhesViewController.swift
//  CinePipoca
//
//  Created by André Haas on 01/06/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import Kingfisher

class EpisodioDetalhesViewController: GenericViewController {
    @IBOutlet weak var tagAssistido: UIImageView!
    
    @IBOutlet weak var imagemPoster: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var ep: Episodio!
    var jaAssistido = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preparePoster()
        self.tagAssistido.isHidden = !(jaAssistido)
        self.tagAssistido.layer.zPosition = 999
        self.title = ep.name
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
    }

    func preparePoster(){
        let urlString = Constants.urlImage + (ep.still_path)!
        
        let url = URL(string: urlString)
        
        imagemPoster.kf.setImage(with: url, placeholder: UIImage(named: "carregando"), options: nil, progressBlock: nil) { (imagem, error, cacheType, urlImagem) in
            if error != nil {
                self.imagemPoster.image = UIImage(named: "semImages")
            }
        }
        
    }
    
}
extension EpisodioDetalhesViewController : UITableViewDelegate, UITableViewDataSource {
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 200.0
        }else{
            return 50.0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0: // Data de exibição
            
            let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
            
            cell.detailTextLabel?.text = DateHelper.FormatarStringData(dataString: ep.air_date)
            cell.textLabel?.text = "Data exibição"
            
            return cell
        case 1:
            let cell = UITableViewCell()
            cell.textLabel?.text = ep.overview
            //cell.textLabel?.lineBreakMode = .byWordWrapping
            cell.textLabel?.minimumScaleFactor = 0.5
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.clipsToBounds = true
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            return cell
            
        case 2:
            let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
            
            cell.textLabel?.text = "Nota"
            cell.detailTextLabel?.text = "\(ep.vote_average)"
            return cell
            
        case 3:
            let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
            
            cell.textLabel?.text = "Qtda Nota"
            cell.detailTextLabel?.text = "\(ep.vote_count)"
            return cell
        case 4:
            let cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: nil)
            
            cell.textLabel?.text = "Episódio"
            cell.detailTextLabel?.text = "S\(ep.season_number)E\(ep.episode_number)"
            return cell
        default:
            break
        }
        
        return UITableViewCell()
    }
    
    
}
