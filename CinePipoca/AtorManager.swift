//
//  AtorManager.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import Alamofire

class AtorManager {
   
     class func loadAtor(movieId: Int = 0, onComplete: @escaping (CapaAtor?)-> Void){
        // https://api.themoviedb.org/3/movie/346364/credits?api_key=8fb08dee4651c3d9b1536a69ea3f3d16
      var urlString = Constants.urlBase
        urlString += "movie/"
        urlString += "\(movieId)/"
        urlString += "credits?api_key=\(Constants.chaveAcesso)"
        DispatchQueue(label: "Teste").async {
            Alamofire.request(urlString).responseJSON { (response) in
                guard let response = response.data else {
                    onComplete(nil)
                    return
                }
                do{
                    let ator = try JSONDecoder().decode(CapaAtor.self, from: response)
                    onComplete(ator)
                }catch{
                    print(error.localizedDescription)
                    onComplete(nil)
                }
            }
        }

        
    }

}
