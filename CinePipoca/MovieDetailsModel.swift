//
//  MovieDetailsModel.swift
//  CinePipoca
//
//  Created by André Haas on 28/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import UIKit

enum TypeMovie: Int {
    
    case pre = 1
    case cineLimitado
    case cine
    case digital
    case fisica
    case tv
    
    func tipo()-> String {
        switch self.rawValue {
        case 1:
            return "Pré estreia"
        case 2:
            return "Cinema (Limitado)"
        case 3:
            return "Cinema"
        case 4:
            return "Filme Digital"
        case 5:
            return "Midia Fisica"
        case 6:
            return "TV"
        default:
            return "Sem data"
        }
    }
}

struct MovieDetailsModel : Decodable {
    let  adult: Bool 
    let  backdrop_path : String?
    let belongs_to_collection : BelongsToCollection?
    let budget : Int
    let genres : [Genero]
    let homepage : String?
    let id :Int
    let imdb_id : String?
    let original_language : String
    let original_title : String
    let overview: String?
    let popularity : Double
    let poster_path : String?
    let production_companies : [ProductionCompanies]
    let production_countries :[ProductionCountries]
    let release_date : String
    let revenue :Int
    let runtime :Int
    let spoken_languages :[SpokenLanguages]
    let status :String
    let tagline : String?
    let title : String
    let video : Bool
    let vote_average : Double
    let vote_count : Int
    let release_dates : ReleaseDatesResult
}

struct ProductionCompanies : Decodable {
    let id : Int
    let logo_path: String?
    let name: String?
    let origin_country: String?
}
struct ProductionCountries : Decodable {
    let iso_3166_1 : String
    let name : String
}
struct SpokenLanguages : Decodable {
    let iso_639_1 : String
    let name : String
}

struct ReleaseDatesResult : Decodable {
    let results : [ReleaseDates]
}

struct ReleaseDates : Decodable {
    let  iso_3166_1 : String
    let release_dates : [ReleaseDate]
}

struct ReleaseDate : Decodable {
    let certification : String
    let iso_639_1 : String
    let release_date  : String
    let type : Int
}

struct BelongsToCollection : Decodable {
    let backdrop_path : String?
    let id : Int
    let name: String?
    let poster_path: String?
}

/*
"backdrop_path" = "/23fRJTMFRG2lMvvpHcIZ0ZU77I1.jpg";
id = 344830;
name = "Cinquenta Tons";
"poster_path" = "/oJrMaAhQlV5K9QFhulFehTn7JVn.jpg";
};
 
 */
