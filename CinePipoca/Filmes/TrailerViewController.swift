//
//  TrailerViewController.swift
//  CinePipoca
//
//  Created by André Haas on 11/04/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import AVKit

class TrailerViewController: UIViewController {
    
    @IBOutlet weak var collection: UICollectionView!
    
    var idFilme: Int!
    let manager = CineMananger.share
    var videos : [VideoResult] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collection.register(UINib(nibName: "TrailerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CellID")
    
        self.getVideos()
    }
    
    func getVideos(){
        manager.getVideo(self.idFilme) { (retorno) in
            if retorno != nil {
                self.videos = (retorno?.results)!
                self.collection.delegate = self
                self.collection.dataSource = self
            }
        }
    }
    
    
}

extension TrailerViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as! TrailerCollectionViewCell
        let video = self.videos[indexPath.item]
        
        let videoURL = URL(string: video.getUrlVideo())
        let videoRequest = URLRequest(url: videoURL!)
        
        cell.webView.loadRequest(videoRequest)
        cell.labelNome.text = video.name
        
        return cell
    }
    
    
}




