//
//  InfoViewController.swift
//  CinePipoca
//
//  Created by André Haas on 02/04/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var overView: UILabel!
    var movieInfo : MovieDetailsModel!
    
    @IBOutlet weak var collectionCompanies: UICollectionView!
    @IBOutlet weak var orcamento: UILabel!
    @IBOutlet weak var realizadoPor: UILabel!
    @IBOutlet weak var lancamentoDVD: UILabel!
    @IBOutlet weak var lancamentoCinema: UILabel!
   
    @IBOutlet weak var labelBilheteria: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        self.collectionCompanies.delegate = self
        self.collectionCompanies.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareView(){
        self.overView.text = movieInfo.overview
        self.orcamento.text = String(movieInfo.budget)
        self.labelBilheteria.text = String(movieInfo.revenue)
        
        print(movieInfo)
        for result in movieInfo.release_dates.results {
            if result.iso_3166_1 == "BR"{
                for release in result.release_dates {
                    if TypeMovie(rawValue: release.type)?.tipo() == TypeMovie.cine.tipo() {
                        let strDate = DateHelper.FormatarStringDataZ(dataString: release.release_date)
                        
                        self.lancamentoCinema.text = "\(strDate)"
                    }else  if TypeMovie(rawValue: release.type)?.tipo() == TypeMovie.digital.tipo(){
                        let strDate = DateHelper.FormatarStringDataZ(dataString: release.release_date)
                        self.lancamentoDVD.text = "\(strDate)"
                    }
                }
            }
        }
        self.lancamentoCinema.font.withSize(15.0)
        self.lancamentoDVD.font.withSize(15.0)
    }
    
    //
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     
     for result in movieDetalhes.release_dates.results {
     
     
     }
     
     
     var totalVotos : String = ""
     
     for _ in 0...Int(movieDetalhes.vote_average){
     totalVotos  += "⭐️ "
     }
     // self.lbNota.text = totalVotos
     
     //  lbQtdaVotos.text = String(movieDetalhes.vote_count)
     //  lbLinguagemOriginal.text = movieDetalhes.original_language
     //  lbAdulto.text = movieDetalhes.adult ? "Sim": "Não"
     //  lbOverView.text = movieDetalhes.overview
     
     */
    
}
extension InfoViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieInfo.production_companies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionCompanies.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath) as! ProductionCompaniesCollectionViewCell
        let companies = self.movieInfo.production_companies[indexPath.item]
        
        cell.nameCompanies.text = companies.name
       // cell.logoView.kf.setImage(with: URL(string: Constants.urlImage + companies.logo_path!)!)
        
        if companies.logo_path != nil {
            cell.logoView.kf.setImage(with: URL(string: Constants.urlImage + companies.logo_path!)!,
                                      placeholder: UIImage(named: "carregando"), options: nil, progressBlock: nil) { (imagem, error, cacheType, urlImagem) in
                                        if error != nil {
                                            cell.logoView.image = UIImage(named: "semImages")
                                        }
            }
        }else{
            cell.logoView.image = UIImage(named: "semImages")
        }
        
        return  cell
    }
    
    
}
