//
//  ElencoViewController.swift
//  CinePipoca
//
//  Created by André Haas on 02/04/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import Kingfisher

class ElencoViewController: UIViewController {

    
    var atores : [Ator]!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        if atores.count > 0 {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTableView(){
       
        tableView.register(UINib(nibName: "AtorTableViewCell", bundle: nil), forCellReuseIdentifier: "AtorTableViewCellID")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ElencoViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if atores.count > 0 {
           return atores.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AtorTableViewCellID", for: indexPath) as! AtorTableViewCell
        let ator = atores[indexPath.row]
      
        cell.imageAtor.layer.cornerRadius = cell.imageAtor.bounds.height/2
        cell.imageAtor.clipsToBounds = true
        
        
        if let urlAtor =  ator.profile_path {
        let url = Constants.urlImage + urlAtor
        cell.imageAtor.kf.setImage(with: URL(string: url)!, placeholder: nil, options: nil, progressBlock: nil) { (imagem, error, cacheRType, imageUrl) in
            if error != nil {
                cell.imageAtor.image = UIImage(named: "semImages")
            }
        }
        }else{
            cell.imageAtor.image = UIImage(named: "semImages")
        }
        cell.nameAtor.text = ator.name
        cell.personagemAtor.text = ator.character
        
        return cell
    }
}
