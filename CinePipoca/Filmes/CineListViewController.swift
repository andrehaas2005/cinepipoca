//
//  CineTableViewController.swift
//  CinePipoca
//
//  Created by André Haas on 23/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class CineListViewController: GenericViewController {
    
    var movies : [Result] = []
    var total = 0
    var currentPage = 1
    var loadingMovie = false
    var atores : [Ator] = []
    var showSearch = false
    var isFavoritos = false
    var isMenuOpen = false
    var textBusca = ""
    let presenterCine = CinePresenter(cineService: CineMananger())
    
    @IBOutlet weak var constraintTable: NSLayoutConstraint!
    
    @IBOutlet weak var viewLoad: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var btSearch: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintTable.constant = -56.0
        presenterCine.attach(self)
        search.isHidden = !showSearch
        search.delegate = self
        self.viewLoad.isHidden = false
        
    }
    
    
    
    
    @IBAction func menuButton(_ sender: Any?){
        
        if isMenuOpen {
            super.menuClose()
        }else{
            super.menuOpen()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        super.chamaMenu()
        title = "Lista de Filmes - Popular"
        loadMovie()
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        search.isHidden = showSearch
        showSearch = !showSearch
        if showSearch {
            constraintTable.constant = 0.0
        }else{
            constraintTable.constant = -56.0
        }
    }
    
    @IBAction func actionMenu(_ sender: Any) {
        
        let alert = UIAlertController(title: "Opções", message: nil, preferredStyle: .actionSheet)
        let actFavoritos = UIAlertAction(title: "Favoritos", style: .default) { (action) in
            self.showHud()
            self.movies  = []
            self.total = 0
            self.currentPage = 1
            self.isFavoritos = true
            self.loadFavoritos()
        }
        
        let actPopular = UIAlertAction(title: "Popular", style: .default) { (action) in
            self.showHud()
            self.movies  = []
            self.total = 0
            self.currentPage = 1
            self.isFavoritos = false
            self.loadMovie()
        }
        let actListaInteresse = UIAlertAction(title: "Lista Interesse", style: .default) { (action) in
            self.showHud()
            self.movies  = []
            self.total = 0
            self.currentPage = 1
            self.isFavoritos = false
            self.loadInteresse()
        }
        
        let actNosCinemas = UIAlertAction(title: "nos CINEMAS", style: .default) { (action) in
            self.showHud()
            self.movies  = []
            self.total = 0
            self.currentPage = 1
            self.isFavoritos = false
            self.loadNosCinemas()
        }
        
        let actProximosLancamentos = UIAlertAction(title: "Próximos Lançamentos", style: .default) { (action) in
            self.showHud()
            self.movies  = []
            self.total = 0
            self.currentPage = 1
            self.isFavoritos = false
            self.buscaProximosLanc()
        }
        
        let actCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alert.addAction(actProximosLancamentos)
        alert.addAction(actNosCinemas)
        alert.addAction(actFavoritos)
        alert.addAction(actListaInteresse)
        alert.addAction(actPopular)
        alert.addAction(actCancel)
        
        present(alert, animated: true, completion: nil)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetalheMovie" {
            let vc = segue.destination as! CineViewController
            vc.movie = sender as! Result
            vc.atores = self.atores
        }
    }
}
// MARK: - Table view data source
extension CineListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  movies.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CineTableViewCell
        let movie = movies[indexPath.row]
        cell.prepareMovie(movie)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (movies.count) - 10 && !loadingMovie && movies.count != total {
            currentPage += 1
            
            if (title?.contains("CINEMAS"))!{
                loadNosCinemas()
            }else if (title?.contains("Busca"))!{
                buscaMovie(page: currentPage, texto: self.textBusca)
            }else if (title?.contains("Próximos"))!{
                buscaProximosLanc()
            }else{
                loadMovie()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        if isFavoritos {
            print("deletar")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = movies[indexPath.row]
        performSegue(withIdentifier: "segueDetalheMovie", sender: movie)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CineListViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let texto = searchBar.text {
            self.viewLoad.isHidden = false
            self.loadMovieBySearch(texto)
        }else{
            
            if isFavoritos {
                self.loadFavoritos()
            }else{
                
                self.loadMovie()
            }
        }
        
    }
}

extension CineListViewController : CineView {
    func alert(type: TypeResponse) {
        super.alertGeneric(typeResponse: type, view: self)
    }
    
    
    func loadNosCinemas(){
        loadingMovie = true
        presenterCine.loadMovieNowPlaying(page: self.currentPage) { (info) in
            self.total = info.total_results!
            self.movies += info.results
            self.title = "Nos CINEMAS"
            self.tableview.reloadData()
            self.loadingMovie = false
            self.hideHud()
        }
        
    }
    
    
    func loadInteresse(){
        loadingMovie = true
        presenterCine.listaInteresse(page: self.currentPage) { (info) in
            self.total = info.total_results!
            self.movies += info.results
            self.title = "Lista Interesses"
            self.loadingMovie = false
            self.tableview.reloadData()
            self.hideHud()
        }
    }
    
    func loadFavoritos(){
        loadingMovie = true
        presenterCine.loadMovieFavoritos(page: self.currentPage) { (info) in
            self.total = info.total_results!
            self.movies += info.results
            self.title = "Lista Favoritos"
            self.tableview.reloadData()
            self.loadingMovie = false
            self.hideHud()
        }
    }
    
    func buscaProximosLanc(){
        loadingMovie = true
        
        presenterCine.listProximosLancamentos(page: self.currentPage) { (info) in
            self.total = info.total_results!
            self.movies += info.results
            self.title = "Próximos Lançamentos"
            self.loadingMovie = false
            self.tableview.reloadData()
            self.viewLoad.isHidden = true
            self.hideHud()
        }
    }
    
    func loadMovie(){
        loadingMovie = true
        
        presenterCine.loadMoviePopular(page: self.currentPage) { (info) in
            self.total = info.total_results!
            self.movies += info.results
            self.title = "Lista Populares"
            self.loadingMovie = false
            self.tableview.reloadData()
            self.viewLoad.isHidden = true
            self.hideHud()
        }
    }
    
    func buscaMovie(page: Int, texto : String){
        loadingMovie = true
        presenterCine.searchMovie(page: page, query: texto) { (retorno) in
            self.total = retorno.total_results!
            self.movies = retorno.results
            self.loadingMovie = false
            self.tableview.reloadData()
            self.viewLoad.isHidden = true
            self.hideHud()
        }
    }
   
    func loadMovieBySearch(_ texto : String){
        self.title = "Busca : \(texto)"
        self.textBusca = texto
        buscaMovie(page: self.currentPage, texto: texto)
    }
}
