//
//  CineViewController.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import UIImageColors


class CineViewController: GenericViewController {
    
    @IBOutlet weak var carrosselImage: UIScrollView!
    @IBOutlet weak var ivPoster: UIImageView!
    @IBOutlet weak var lbNota: UILabel!
    @IBOutlet weak var conteudoView: UIView!
    
    @IBOutlet weak var lbQtdaVotos: UILabel!
    @IBOutlet weak var lbLinguagemOriginal: UILabel!
    @IBOutlet weak var lbGeneros: UILabel!
    @IBOutlet weak var lbAdulto: UILabel!
    @IBOutlet weak var lbDataLancamento: UILabel!
    @IBOutlet weak var lbOverView: UITextView!
    
    @IBOutlet weak var cvAtores: UICollectionView!
    @IBOutlet weak var topo1: UIView!
    @IBOutlet weak var topo2: UIView!
    @IBOutlet weak var topo3: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var seletor: UISegmentedControl!
    
    var currentViewController : UIViewController!
    var informacaoViewControlle : UIViewController!
    var elentoViewController : UIViewController!
    var trailerViewController: UIViewController!
    var movie: Result!
    var movieDetalhes : MovieDetailsModel!
    var atores : [Ator]! = []
    
   
    
    private let manager = CineMananger.share
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        loadDetails()
        self.seletor.insertSegment(withTitle: "TRAILER", at: 2, animated: true)
        self.seletor.selectedSegmentIndex = 0
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.topItem!.title = " "
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    func setupNavigation(){
        
        self.navigationController?.navigationBar.backgroundColor = self.colorBackGroud
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: self.colorDetail]
        self.title = self.movie.original_title
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    
    @IBAction func AddFavorito(_ sender: UIButton) {
        
        manager.favoriteMovie(id: movie.id)
        
    }
    
    @IBAction func addInteresse(_ sender: UIButton) {
        manager.addListaInteresse(movieId: movie.id)
    }
    
    @IBAction func actionOpcoes(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        
        self.currentViewController?.view.removeFromSuperview()
        self.currentViewController?.removeFromParentViewController()
        displayCurrentTab(sender.selectedSegmentIndex)
        
    }
    
    func displayCurrentTab(_ index: Int){
        if let vc = viewControllerForSelectedSegmentIndex(index) {
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            vc.view.frame = self.conteudoView.bounds
            self.conteudoView.addSubview(vc.view)
            self.currentViewController = vc
            vc.viewWillAppear(true)
        }
    }
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        
        switch index {
        case 0:
            if informacaoViewControlle == nil {
                let view = storyboard?.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
                view.title = "Info"
                view.movieInfo = self.movieDetalhes
                informacaoViewControlle = view
                
            }
            vc = informacaoViewControlle
            break
        case 1:
            
            if elentoViewController == nil {
                let view = storyboard?.instantiateViewController(withIdentifier: "ElencoViewController") as! ElencoViewController
                view.title = "Elenco"
                view.atores = self.atores
                elentoViewController = view
            }
            vc = elentoViewController
            break
        case 2:
            if trailerViewController == nil {
                
                
                let view =  TrailerViewController()
                view.title = "Trailer"
                view.idFilme = self.movieDetalhes.id
                trailerViewController = view
            }
            
            vc = trailerViewController
        default:
            break
            
        }
        return vc
    }
    
    private func loadDetails(){
        self.showHud()
        manager.getMovieDetails(idMovie: movie.id) { (retorno) in
            if retorno != nil {
                self.movieDetalhes = retorno
                self.prepareMovie()
                self.displayCurrentTab(self.seletor.selectedSegmentIndex)
                
            }
        }
    }
    
    
    private func loadImagem(){
        manager.getImageMovie(movie.id) { (retornoImagem) in
            if retornoImagem != nil {
                for backdrop in (retornoImagem?.backdrops)!{
                    let urlStr = "\(Constants.urlImage)\(backdrop.file_path)"
                    print(urlStr)
                    self.carrosselImage.auk.show(url: urlStr)
                    
                    self.carrosselImage.auk.startAutoScroll(delaySeconds: 6.0)
                    self.carrosselImage.auk.settings.contentMode = .scaleAspectFill
                    
                }
            }
        }
        
        self.hideHud()
    }
    
  
    
    func prepareMovie(){
        
        if let poster = movieDetalhes.poster_path {
            let urlString = Constants.urlImage + poster
            if let url = URL(string: urlString) {
                ivPoster.kf.indicatorType = .activity
                ivPoster.kf.setImage(with: url
                , placeholder: UIImage(named: "carregando"), options: nil, progressBlock: nil) { (imagem, error, cacheType, urlImgem) in
                    if error != nil {
                        self.ivPoster.image = UIImage(named: "semImages")
                    }else{
                        self.getCores(self.ivPoster.image!)
                        
                    }
                }
            }else{
                
            }
        }
        
        self.navigationController!.navigationBar.tintColor = self.colorDetail
        
        self.carrosselImage.backgroundColor = self.colorBackGroud
        self.topo2.backgroundColor = self.colorPrimary
        self.topo3.backgroundColor = self.colorSecondary
        self.labelTitle.textColor = self.colorDetail
        self.conteudoView.backgroundColor = self.colorBackGroud
        
        self.labelTitle.text = movieDetalhes.title
        
        var listGenero : [String] = []
        for genero in movieDetalhes.genres {
            listGenero.append(genero.name)
        }
        self.lbGeneros.text = listGenero.joined(separator: " - ")
        self.lbGeneros.font.withSize(17.0)
        self.lbGeneros.lineBreakMode = .byWordWrapping
        self.lbGeneros.numberOfLines = 0
        self.lbGeneros.textColor = colorDetail
        self.seletor.tintColor = colorDetail
        self.getAtorByMovie(self.movieDetalhes.id)
        self.loadImagem()
        
    }
    
    func getAtorByMovie(_ movieId: Int){
        AtorManager.loadAtor(movieId: movieId) { (data) in
            if let ator = data?.cast {
                self.atores = ator
                
                self.hideHud()
            }
            
        }
        
    }
    
    
    func formatarData(_ dataString: String)-> String {
        
        let dataEntrada = DateFormatter()
        dataEntrada.dateFormat = "yyyy/MM/dd"
        let data = dataEntrada.date(from: dataString)
        
        let dataSaida = DateFormatter()
        dataSaida.dateFormat = "dd/MM/yyyy"
        return  dataSaida.string(from: data!)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


