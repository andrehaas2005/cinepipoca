//
//  ResultVideo.swift
//  
//
//  Created by André Haas on 11/04/2018.
//

import Foundation
import UIKit

struct ResultVideoModel: Decodable {
    let id : Int!
    let results : [VideoResult]
    
}
struct VideoResult : Decodable {
    let id : String!
    let iso_639_1 : String
    let iso_3166_1: String
    let key : String
    let name: String
    let site: String
    let size: Int
    let type: String
    
    func getUrlVideo()-> String{
        var urlString = ""
        if self.site.elementsEqual("YouTube"){
            urlString = "\(Constants.urlYoutube)\(self.key)"
        }
        
        return urlString
    }
    
}
