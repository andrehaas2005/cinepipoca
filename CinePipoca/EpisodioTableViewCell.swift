//
//  EpisodioTableViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 06/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class EpisodioTableViewCell: UITableViewCell {

    @IBOutlet weak var episodioPoster: UIImageView!
    
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var fundo: UIView!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tagAssistido: UIImageView!
    @IBOutlet weak var lbEpisodio: UILabel!
    let  urlImage =  Constants.urlImage
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func preparaCell(ep: Episodio, assistido: Bool){
        self.lbDate.text = DateHelper.FormatarStringData(dataString: (ep.air_date))
        self.lbName.text = ep.name
        self.tagAssistido.isHidden = true
        self.lbEpisodio.text = "S\(ep.season_number)E\(ep.episode_number)"
        self.tagAssistido.layer.zPosition = 999
        if assistido {
            self.tagAssistido.isHidden = false
            
        }else{
            self.tagAssistido.isHidden = true
        }
       if ep.still_path != nil {
            self.episodioPoster.kf.setImage(with: URL(string: urlImage + ep.still_path!)!, placeholder: nil, options: nil, progressBlock: nil) { (imagem, error, cacheType, urlImagem) in
                print(error.debugDescription)
            }
        }else{
            self.episodioPoster.image = UIImage(named: "semImages")
        }
        
        self.fundo.layer.cornerRadius = 10.0
        self.fundo.clipsToBounds = true
    }

}
