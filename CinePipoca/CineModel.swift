//
//  CineModel.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct Capa : Decodable {
    let page : Int?
    let total_results : Int?
    let total_pages: Int?
    let results : [Result]

}
//&append_to_response=release_dates
struct Result : Decodable {
    let vote_count: Int
    let id: Int
    let video: Bool
    let vote_average :Double
    let title :String
    let popularity :Double
    let poster_path :String?
    let original_language :String?
    let original_title :String
    let genre_ids : [Int]
    let backdrop_path :String?
    let adult :Bool
    let overview :String?
    let release_date : String
    
   
    
}
