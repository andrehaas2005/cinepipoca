//
//  LoginViewController.swift
//  CinePipoca
//
//  Created by André Haas on 18/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var textUserName: UITextField!
    @IBOutlet weak var textPass: UITextField!
    
    @IBOutlet weak var painelUser: UIView!
    @IBOutlet weak var buttonEntrar: UIButton!
    
    let manager = LoginManager.share
    var token : TokenModel!
    var username: String!
    var password: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        painelUser.layer.borderColor = UIColor.black.cgColor
        painelUser.layer.borderWidth = 1.0
        manager.authentication { (result) in
            if result != nil {
                self.token = result!
                
                UserDefaults.standard.set(self.token.request_token!, forKey: "Token")
                if let token = UserDefaults.standard.string(forKey: "Token"){
                    print(token)
                }
            }
        }

        // Do any additional setup after loading the view.
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ActionLogar(_ sender: UIButton) {
        if validaCampo() {
            
            manager.login(user: self.username, pass: self.password) { (retorno) in
                
                if let validado = retorno as? Bool {
                    if validado {
                         self.performSegue(withIdentifier: "seguePrincipal", sender: nil)
                    }
                }else{
                    if let messagem = retorno as? String {
                        self.alert(title: "Erro", message: messagem)
                    }
                }
               
            }
        }
    }
    
    func validaCampo()-> Bool{
        
        var retorno = false
        if !(textUserName.text?.isEmpty)!{
            self.username = textUserName.text
            retorno = true
        }else{
            return false
        }
        
        if !(textPass.text?.isEmpty)!{
            self.password = textPass.text
            retorno = true
        }else{
            return false
        }
        
        return retorno
    }
    
 
    func alert(title: String , message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "seguePrincipal"{
            //let vc = segue.destination as!  CineListViewController
            //present(vc, animated: true, completion: nil)
        }
    }
    

}
