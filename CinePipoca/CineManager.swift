//  CineManager.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import Alamofire

enum TypeOperacao: String{
    case movie = "movie/"
    case search = "search/"
}

class CineMananger {
    
    static let share = CineMananger()
    private let key = Constants.chaveAcesso
    private  let queue = DispatchQueue(label: "manager-response-queue-chat", qos: .userInitiated, attributes: .concurrent)
    
    private let languageDefault = "pt-BR"
    
   
    
    
    func addListaInteresse(movieId: Int){
        //{{host}}account/6198042/watchlist?api_key={{keyCode}}&session_id=fadc26e185e3a132fe797bf60fb8c3f2308ed7c8
        let session = UserDefaults.standard.object(forKey: "SessionID") as! String
        let urlString = "\(Constants.urlBase)account/\(Constants.idConta)/watchlist?api_key=\(Constants.chaveAcesso)&session_id=\(session)"
        
        let param = ["media_type" : "movie",
                     "media_id" : movieId,
                     "watchlist" : true] as [String : Any]
        
        let url = URL(string: urlString)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        urlRequest.setValue("application/json;charset=utf-8", forHTTPHeaderField: "content-type")
        let request = try! Alamofire.JSONEncoding.default.encode(urlRequest, with: param)
        
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
                
            case .success(let json):
                print(json)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }
    
    
    func favoriteMovie(id: Int!, addFavorito : Bool = true) {
        
        let session = UserDefaults.standard.object(forKey: "SessionID") as! String
        let url = "\(Constants.urlBase)account/6198042/favorite?api_key=\(Constants.chaveAcesso)&session_id=\(session)"
        let urlString = URL(string: url)!
        var mutableURLRequest = URLRequest(url: urlString)
        mutableURLRequest.httpMethod = Alamofire.HTTPMethod.post.rawValue
        mutableURLRequest.setValue("application/json;charset=utf-8", forHTTPHeaderField: "content-type")
        
        let parameters = [
            "media_type": "movie",
            "media_id": id,
            "favorite": addFavorito
            ] as [String : Any]
        
        
        let request = try! Alamofire.JSONEncoding.default.encode(mutableURLRequest, with: parameters)
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
                
            case .success(let json):
                
                if let jsonObjt = json as? [String: Any] {
                    print("status_code :  \(String(describing: jsonObjt["status_message"]))")
                }
                print(json)
            case .failure(let error):
                print(error)
                
            }
        }
    }
    
    func getImageMovie(_ filmeID : Int, onComplete : @escaping(ImageTvModel?)-> Void){
        //{{host}}movie/284053/images?api_key={{keyCode}}
        let url = "\(Constants.urlBase)movie/\(filmeID)/images?api_key=\(Constants.chaveAcesso)"
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            
            do {
                let retorno = try JSONDecoder().decode(ImageTvModel.self, from: data)
                onComplete(retorno)
                return
            }catch{
                onComplete(nil)
                return
            }
        }
        
    }
    // https://api.themoviedb.org/3/movie/354912?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR&append_to_response=release_dates
    func getMovieDetails(idMovie: Int, onComplete : @escaping(MovieDetailsModel?)-> Void ){
        let url = "\(Constants.urlBase)movie/\(idMovie)?api_key=\(Constants.chaveAcesso)&language=pt-BR&append_to_response=release_dates"
        
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            
            do {
                let retorno = try JSONDecoder().decode(MovieDetailsModel.self, from: data)
                onComplete(retorno)
            }catch{
                print("Error: \(error.localizedDescription)")
                onComplete(nil)
                return
            }
        }
    }
}
extension CineMananger {
    //{{host}}movie/337167/videos?api_key={{keyCode}}&language=pt-BR
    func getVideo(_ idFilme: Int, onComplete : @escaping(ResultVideoModel?)-> Void){
        let url = "\(Constants.urlBase)movie/\(idFilme)/videos?api_key=\(Constants.chaveAcesso)&languale=pt-BR"
        
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do {
                let retorno = try JSONDecoder().decode(ResultVideoModel.self, from: data)
                onComplete(retorno)
            }catch{
                print("Error : \(error.localizedDescription)")
                onComplete(nil)
                return
            }
        }
    }
    
    
}


extension CineMananger { //Novo formato
    
    
    func loadMovieNowPlaying(page: Int = 1, onComplete: @escaping(TypeResponse)->Void){
        
        let parameters : [String : Any] = ["api_key" : key, "language" : languageDefault , "page" : page, "region" : "BR"  ]
        
        
        Alamofire.request(CinePipocaAPI.Router.getMoviesNowPlaying(parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
                
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let movies = try JSONDecoder().decode(Capa.self, from: data)
                            onComplete(.Sucesso(movies))
                            
                        }catch{
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
    
    func loadMovieFavoritos(page: Int = 1, onComplete: @escaping(TypeResponse)-> Void){
        guard let sessionID = UserDefaults.standard.object(forKey: Constants.Key.SessionID) else {
            
            return
        }
        
        let parameters : [String : Any] = ["api_key" : key, "session_id" : sessionID, "sort_by" : "created_at.asc", "page" : page, "language" :  languageDefault]
        let acountId = Constants.idConta
        
        Alamofire.request(CinePipocaAPI.Router.getMovieFavoritos(acountId, parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
                
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let retorno = try JSONDecoder().decode(Capa.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }catch {
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
   
    func loadMoviePopular(page: Int = 0, onComplete: @escaping (TypeResponse)-> Void){
        
        let parameters : [String : Any] = ["api_key" : key,"language" : languageDefault, "page" : page]
        
        Alamofire.request(CinePipocaAPI.Router.getListaMoviePopular(parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let retorno = try JSONDecoder().decode(Capa.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }catch {
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
    func searchMovie(page: Int = 1 , query: String, onComplete: @escaping (TypeResponse)-> Void){
        let queryString = query.replacingOccurrences(of: " ", with: "-")
        
        let param : [String : Any] = ["api_key" : key, "language" : languageDefault,"query" : queryString, "page" : page]
        
        Alamofire.request(CinePipocaAPI.Router.searchMovie(param)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let retorno = try JSONDecoder().decode(Capa.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }catch {
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
    func listaInteresse(page: Int = 1, onComplete: @escaping(TypeResponse)-> Void){
        let session = UserDefaults.standard.object(forKey: Constants.Key.SessionID) as! String
        let acoutID = Constants.idConta
        let parameters : [String : Any] = ["api_key":key, "language" : languageDefault, "session_id" : session, "sort_by" : "created_at.asc", "page" : page ]
        
        
        Alamofire.request(CinePipocaAPI.Router.getListaInteresse(acoutID, parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let retorno = try JSONDecoder().decode(Capa.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }catch {
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
    func listProximosLancamentos(page : Int = 1, onComplet : @escaping(TypeResponse)->Void){
        
        let parameters : [String : Any] = ["api_key" : key, "language" : languageDefault, "page" : page, "region": "BR"]
        
        Alamofire.request(CinePipocaAPI.Router.getUpcomingMovie(parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let retorno = try JSONDecoder().decode(Capa.self, from: data)
                            onComplet(.Sucesso(retorno))
                        }catch {
                            onComplet(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet {
                    onComplet(.SemInternet)
                }else{
                    onComplet(.Error(error))
                }
            }
        }
        
    }
    
}

/*
 https://api.themoviedb.org/3/movie/now_playing?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR&page=1&region=BR
 */

 //https://api.themoviedb.org/3/movie/popular?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR&page=3
//https://api.themoviedb.org/3/tv/63174/season/2?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR

//https://api.themoviedb.org/3/tv/63174/season/3?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR

 //https://api.themoviedb.org/3/account/{account_id}/watchlist/movies?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR&session_id=fadc26e185e3a132fe797bf60fb8c3f2308ed7c8&sort_by=created_at.asc&page=1
 //https://api.themoviedb.org/3/search/movie?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR&query=Homem-Aranha&page=1&include_adult=false
