//
//  ProductionCompaniesCollectionViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 10/04/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class ProductionCompaniesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var nameCompanies: UILabel!
    
}
