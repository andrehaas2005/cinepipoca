//
//  CineTableViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import Kingfisher

class CineTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivCapa: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var lbVoto: UILabel!
    @IBOutlet weak var lbRelese: UILabel!
    @IBOutlet weak var lbResumo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepareMovie(_ movie: Result){
        
        if let poster = movie.poster_path {
            let urlString = Constants.urlImage + poster
            if let url = URL(string: urlString) {
                ivCapa.kf.indicatorType = .activity
                ivCapa.kf.setImage(with: url)
            }else{
                self.ivCapa.image = UIImage(named: "semImages")
            }
        }else{
            self.ivCapa.image = UIImage(named: "semImages")
        }
        self.lbTitle.text = movie.title
        if !movie.release_date.isEmpty {
        //self.lbRelese.text = "Lançamento: \(formatarData(movie.release_date))"
            self.lbRelese.text = "Lançamento: \(DateHelper.FormatarStringData(dataString: movie.release_date))"
        }else{
            self.lbRelese.text = "Sem Data"
        }
        if movie.overview != nil && !(movie.overview?.isEmpty)! {
            self.lbResumo.text = movie.overview
        }else{
            self.lbResumo.text = "Sem OverView"
        }
        self.lbVoto.text = "Nota: \(movie.vote_average)"
    }

}
