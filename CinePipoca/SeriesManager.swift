//
//  SeriesManager.swift
//  CinePipoca
//
//  Created by André Haas on 26/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class SeriesManager: NSObject {
    
 
    private let key = Constants.chaveAcesso
    private  let queue = DispatchQueue(label: "manager-response-queue-chat", qos: .userInitiated, attributes: .concurrent)
    private let languageDefault = "pt-BR"
    
    
  static  let share = SeriesManager()
    
    
    
    func getSeriesNoAr(page: Int = 1, onComplete: @escaping(TypeResponse)->Void){
        
        let parameters : [String : Any] = ["api_key" : key, "language" : languageDefault, "page" : page]
        
        Alamofire.request(CinePipocaAPI.Router.getSeriesNoAr(parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result{
                
            case .success(_):
                if response.response?.statusCode == 200 {
                    do{
                        if let data = response.data {
                        
                        let retorno = try JSONDecoder().decode(CapaSerie.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }
                    }catch{
                        onComplete(.Error(error))
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet{
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }

    }
    
    
     func getSeries(page: Int = 1, onComplete: @escaping (TypeResponse)-> Void){
        
        let parameters : [String : Any] = ["page" : page, "language" : languageDefault, "api_key" : key]
        
        Alamofire.request(CinePipocaAPI.Router.getSeries(parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result{
                
            case .success(_):
                if response.response?.statusCode == 200 {
                    if let  data =  response.data {
                        do{
                            let retorno = try JSONDecoder().decode(CapaSerie.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }catch{
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet{
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
     func searchSeries(page: Int = 1, query: String, onComplete: @escaping (TypeResponse)-> Void ){
        //https://api.themoviedb.org/3/search/tv?page=1&query=SuperNatural&language=pt-BR&api_key=8fb08dee4651c3d9b1536a69ea3f3d16'
         let strQuery = query.replacingOccurrences(of: " ", with: "-")
        let parameters : [String : Any] = ["page": page, "query" : strQuery, "language" : languageDefault, "api_key" : key]
        
        Alamofire.request(CinePipocaAPI.Router.searchSeriesByName(parameters)).validate().responseJSON(queue: queue, options: JSONSerialization.ReadingOptions.allowFragments) { (response) in
            switch response.result {
                
            case .success(let json):
                print(json)
                if response.response?.statusCode == 200 {
                    if let data = response.data {
                        do{
                            let retorno = try JSONDecoder().decode(CapaSerie.self, from: data)
                            onComplete(.Sucesso(retorno))
                        }catch {
                            onComplete(.Error(error))
                        }
                    }
                }
            case .failure(let error):
                if let err = error as? URLError, err.code == URLError.notConnectedToInternet{
                    onComplete(.SemInternet)
                }else{
                    onComplete(.Error(error))
                }
            }
        }
    }
    
     func getSerieById(serie_id : Int,onComplete: @escaping (DetailSerieModel?) -> Void){
        //https://api.themoviedb.org/3/tv/63174?language=pt-BR&api_key=8fb08dee4651c3d9b1536a69ea3f3d16
        var urlString = Constants.urlBase
        urlString += "tv/\(serie_id)?language=pt_BR&api_key=\(Constants.chaveAcesso)"
       // print(urlString)
        Alamofire.request(urlString).responseJSON { (response) in
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do{
                let retorno = try JSONDecoder().decode(DetailSerieModel.self, from: data)
                onComplete(retorno)
            }catch{
                print(error.localizedDescription)
                onComplete(nil)
            }
        }
        
    }
    
     func getImagensSerie(serie_id: Int, onComplete: @escaping(ImageTvModel?)->Void) {
        //https://api.themoviedb.org/3/tv/63174/images?api_key=8fb08dee4651c3d9b1536a69ea3f3d16
        var urlString = Constants.urlBase
        urlString += "tv/\(serie_id)/images?api_key=\(Constants.chaveAcesso)"
        print(urlString)
        Alamofire.request(urlString).responseJSON { (response) in
            guard let data = response.data else{
                onComplete(nil)
                return
            }
            do{
                let retorno = try JSONDecoder().decode(ImageTvModel.self, from: data)
                onComplete(retorno)
            }catch{
                onComplete(nil)
            }
        }
    }
    
    
     func getEpisodio(serie_id: Int,season: Int, onComplete: @escaping(Season?)-> Void){
        //https://api.themoviedb.org/3/tv/63174/season/1?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&language=pt-BR
       
        var urlString = Constants.urlBase
        urlString += "tv/\(serie_id)/season/\(season)?api_key=\(Constants.chaveAcesso)&language=pt-BR"
        print(urlString)
        Alamofire.request(urlString).responseJSON { (response) in
//            print(response.result)
//            print(response.data)
            guard let data = response.data else {
                onComplete(nil)
                return
            }
            do {
                let retorno = try JSONDecoder().decode(Season.self, from: data)
                onComplete(retorno)
            }catch{
                print(error.localizedDescription)
                onComplete(nil)
            }
        }
        
        
    }
    
}
