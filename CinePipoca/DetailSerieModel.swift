//
//  DetailSerieModel.swift
//  CinePipoca
//
//  Created by André Haas on 29/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct DetailSerieModel : Decodable {
    let backdrop_path : String
   let created_by: [ Criador ]
    let episode_run_time :[Int]
   let first_air_date : String
   let genres :[Genero]
    let homepage : String?
    let id : Int
    let in_production :Bool?
    let languages : [String]
    let last_air_date : String
    let name : String
    let networks :[NetWorks]
   let number_of_episodes : Int
    let number_of_seasons : Int
    let origin_country : [String]
    let original_language : String
    let original_name : String
    let overview : String
    let popularity : Double
    let poster_path : String?
    let production_companies : [Producao]
    let seasons :[Seasons]
    let status : String
    let type : String
    let vote_average : Double
    let vote_count : Int
}

struct Criador : Decodable {
    let id :Int
    let name :String
    let profile_path : String?
}

struct Genero : Decodable {
    let id : Int
    let name : String
}
struct NetWorks : Decodable {
    let id : Int
    let name : String
}

struct Producao : Decodable {
    let name : String
    let id : Int
}
struct Seasons : Decodable {
    let air_date : String?
    let episode_count : Int
    let id : Int
    let poster_path : String?
    let season_number : Int
    
}
