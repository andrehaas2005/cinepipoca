//
//  AppDelegate.swift
//  CinePipoca
//
//  Created by André Haas on 23/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var user : String!
    var window: UIWindow?
    let center = UNUserNotificationCenter.current()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       
      FirebaseApp.configure()
        
        
        center.delegate = self
        center.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .notDetermined {
                
                let options : UNAuthorizationOptions = [.alert, .sound,.badge, .carPlay]
                
                self.center.requestAuthorization(options: options, completionHandler: { (success, error) in
                    if error == nil {
                        print(success)
                    }else{
                        print(error!.localizedDescription)
                    }
                })
                
                
            }else if settings.authorizationStatus == .denied {
                print("Usuario nãou autorizou")
            }
            
            let nsConfirm = UNNotificationAction(identifier: "Confirm", title: "Assistir hoje", options: [.foreground])
            
            let nsAdiar = UNNotificationAction(identifier: "Adiar", title: "Lembrar depois", options: [])
            
            if #available(iOS 11.0, *) {
                let category = UNNotificationCategory(identifier: "Lembrete", actions: [nsConfirm,nsAdiar], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: [.customDismissAction])
            } else {
                // Fallback on earlier versions
            }
            if #available(iOS 11.0, *) {
                let category = UNNotificationCategory(identifier: "Lembrete", actions: [nsConfirm,nsAdiar], intentIdentifiers: [], hiddenPreviewsBodyPlaceholder: "", options: [.customDismissAction])
                 self.center.setNotificationCategories([category])
            } else {
                // Fallback on earlier versions
            }
            
           
        }
        self.validaSession()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        print("App Finalizado")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func validaSession(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController: UIViewController
     
        
        if  let sessionId = UserDefaults.standard.object(forKey: "SessionID") as? String {
           print("SessionID : \(sessionId)")
            
            
            if let usuarioData = UserDefaults.standard.object(forKey: "User") as? Data {
                let usuarioInfo = try! JSONDecoder().decode(UserInfo.self, from: usuarioData)
                
                self.user = usuarioInfo.name
            }else{
                LoginManager.share.getInfoConta()
            }
            viewController = storyboard.instantiateViewController(withIdentifier: "CineListNavigationController")
        
        }else{
       
             viewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
     
        }
        
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
    }


}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    //Dispara notificação com aplicativo aberto
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.badge,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
     
        var id = response.notification.request.identifier
        print("id : \(id) - dentro do didReceive response")
        
        switch response.actionIdentifier {
        case "Confirm":
            print("Usuario vai assistir hoje")
        case "Adiar":
            print("Agendar notificação para mais tarde")
            
        case UNNotificationDefaultActionIdentifier:
            print("Tocou na propria notificação")
        case UNNotificationDismissActionIdentifier:
            print("Usuario deu dismiss")
            
        default:
            break
        }
        
        completionHandler()
    }
}

