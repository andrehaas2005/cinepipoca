//
//  AtorTableViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 02/04/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class AtorTableViewCell: UITableViewCell {

    @IBOutlet weak var imageAtor: UIImageView!
    @IBOutlet weak var nameAtor: UILabel!
    @IBOutlet weak var personagemAtor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
