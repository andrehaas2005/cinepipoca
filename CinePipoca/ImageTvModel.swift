//
//  ImageTvModel.swift
//  CinePipoca
//
//  Created by André Haas on 14/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation


struct ImageTvModel :  Decodable {
    
    let backdrops : [Backdrop]
    let id: Int
    let posters : [Poster]
}


struct Backdrop : Decodable {
    
    let aspect_ratio : Float
    let file_path : String
    let height : Int
    let iso_639_1 : String?
    let vote_average: Double
    let vote_count: Int
    let width: Int

}

struct Poster : Decodable {
    
    let aspect_ratio: Float
    let file_path: String
    let height: Int
    let iso_639_1: String?
    let vote_average: Double
    let vote_count: Int
    let width: Int

}
