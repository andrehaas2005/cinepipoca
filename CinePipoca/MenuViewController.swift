//
//  MenuViewController.swift
//  CinePipoca
//
//  Created by André Haas on 19/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class MenuViewController: GenericViewController {
    @IBOutlet weak var contTopImage: NSLayoutConstraint!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var imagePerfil: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
     var userInfo : UserInfo?
    
    var itensMenu = ["Filmes","Séries","Pessoas Populares","Lembretes", "Contate-nos", "Comunidade Google+","Desbloquear a versão Pro", "Configurações", "Sair"]
    
    var iconMenu = ["iconCinema", "tv","robin-williams","calendar-clock", "email", "google-plus-logo", "padlock", "settings" , "exit"]

    override func viewDidLoad() {
        super.viewDidLoad()
        getUser()
        setupTop()
        prepareMenu()
    }


    func getUser(){
        if let userData = UserDefaults.standard.object(forKey: "User") as? Data {
            let user = try! JSONDecoder().decode(UserInfo.self, from: userData)
            self.userInfo = user
            self.labelName.text = userInfo?.name
        }
    }
    
    
    func setupTop(){
        let topDistancia = viewTop.bounds.size.height
        contTopImage.constant = topDistancia / 2
        imagePerfil.layer.cornerRadius = imagePerfil.bounds.size.height / 2
        imagePerfil.image = UIImage(named: "imageEmptyPerfil")
        imagePerfil.clipsToBounds = true
    }

    
    func prepareMenu(){
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.closeMenu))
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.closeMenu))
        swipeLeft.direction = .left
        swipeRight.direction = .right
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
    }
    
    
    @objc func closeMenu(){
        UIView.animate(withDuration: 0.6) {
            self.view.removeFromSuperview()
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
   */

}
extension MenuViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itensMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MenuTableViewCell
        let decrMenu = itensMenu[indexPath.row]
        let icon = iconMenu[indexPath.row]
        cell.labelMenu.text = decrMenu
        cell.iconMenu.image = UIImage(named: icon)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       tableView.deselectRow(at: indexPath, animated: true)
        let opcao = itensMenu[indexPath.row]
        switch opcao {
        case "Filmes":
            performSegue(withIdentifier: "SegueFilmes", sender: nil)
        case "Séries":
            performSegue(withIdentifier: "SegueSeries", sender: nil)
        case "Sair":
            logOff()
            break
        default:
            break
        }
        closeMenu()
    
    }
    
    func logOff(){
        UserDefaults.standard.removeObject(forKey: "SessionID")
        UserDefaults.standard.removeObject(forKey: "Token")
        performSegue(withIdentifier: "SegueLogin", sender: nil)
       
    }
}
