//
//  CInePipocaAPI.swift
//  CinePipoca
//
//  Created by André Haas on 31/05/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import Alamofire


class CinePipocaAPI {
    
    enum Router : URLRequestConvertible {
        
        case getMoviesNowPlaying([String : Any])
        case getMovieFavoritos(String, [String: Any])
        case getListaInteresse(String,[String:Any])
        case getListaMoviePopular([String:Any])
        case searchMovie([String:Any])
        case getUpcomingMovie([String:Any])
        case getSeriesNoAr([String : Any])
        case getSeries([String : Any])
        case searchSeriesByName([String : Any])
        
        var method : Alamofire.HTTPMethod {
            switch self {
                
            case .getMoviesNowPlaying(_):
                return .get
                
            case .getMovieFavoritos(_, _):
                return .get
            case .getListaInteresse(_, _):
                return .get
            case .getListaMoviePopular(_):
                return .get
            case .searchMovie(_):
                return .get
            case .getUpcomingMovie(_):
                return .get
            case .getSeriesNoAr(_):
                return .get
            case .getSeries(_):
                return .get
            case .searchSeriesByName(_):
                return .get
            }
        }
        
        var path : String {
            switch self {
                
            case .getMoviesNowPlaying(_):
                return "movie/now_playing"
            case .getMovieFavoritos(let account_id, _):
                return "account/\(account_id)/favorite/movies"
            case .getListaInteresse(let account_id, _):
                return "account/\(account_id)/watchlist/movies"
            case .getListaMoviePopular(_):
                return "movie/popular"
            case .searchMovie(_):
                return "search/movie"
            case .getUpcomingMovie(_):
                return "movie/upcoming"
            case .getSeriesNoAr(_):
                return "tv/on_the_air"
            case .getSeries(_):
                return "tv/popular"
            case .searchSeriesByName(_):
                return "search/tv"
            }
        }
        
        func asURLRequest() throws -> URLRequest {
            let url = NSURL(string: Constants.urlBase)!
            var mutableURLRequest = URLRequest(url: url.appendingPathComponent(path)!)
            mutableURLRequest.httpMethod = method.rawValue
            
            switch self {
                
            case .getMoviesNowPlaying(let parameters):
                return try URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .getMovieFavoritos(_, let parameters):
                return try URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .getListaInteresse(_, let parameters):
                return try URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .getListaMoviePopular(let parameters):
                return try URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .searchMovie(let parameters):
                return try URLEncoding.default.encode(mutableURLRequest, with: parameters)
            case .getUpcomingMovie(let param):
                return try URLEncoding.default.encode(mutableURLRequest, with: param)
            case .getSeriesNoAr(let param):
                return try URLEncoding.default.encode(mutableURLRequest, with: param)
            case .getSeries(let param):
                return try URLEncoding.default.encode(mutableURLRequest, with: param)
            case .searchSeriesByName(let param):
                return try URLEncoding.default.encode(mutableURLRequest, with: param)
            }
            
            
        }
        
    }
    
}
