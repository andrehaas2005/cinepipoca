//
//  LoginManager.swift
//  CinePipoca
//
//  Created by André Haas on 18/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import Alamofire
//{{host}}authentication/token/new?api_key={{keyCode}}
class LoginManager: NSObject {
    private override init() {
        
    }
    
    static let share = LoginManager()
    
    var token : String!
    
    func authentication(onComplete: @escaping(TokenModel?)-> Void){
        let url = "\(Constants.urlBase)authentication/token/new?api_key=\(Constants.chaveAcesso)"
        print(url)
        Alamofire.request(url).responseJSON { (response) in
            switch response.result{
                
            case .success(let json):
                print(json)
                do{
                let retorno = try JSONDecoder().decode(TokenModel.self, from: response.data!)
                    onComplete(retorno)
                }catch{
                    print(error.localizedDescription)
                    onComplete(nil)
                }
            case .failure(let error):
                print(error)
                onComplete(nil)
            }
        }
        
    }
    
    func getInfoConta(){
        //https://api.themoviedb.org/3/account?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&session_id=492fdc8ce82797871b8d528d0b616dd12b335645
        let sessionID = UserDefaults.standard.object(forKey: "SessionID") as! String
        let url = "\(Constants.urlBase)account?api_key=\(Constants.chaveAcesso)&session_id=\(sessionID)"
        
        Alamofire.request(url).responseJSON { (response) in
            guard let data = response.data else {return}
            do{
            let user = try JSONDecoder().decode(UserInfo.self, from: data)
                if !user.name.isEmpty{
                    
                    UserDefaults.standard.removeObject(forKey: Constants.Key.User)
                    UserDefaults.standard.set(data, forKey: Constants.Key.User)
                    UserDefaults.standard.removeObject(forKey: Constants.Key.AccountId)
                    UserDefaults.standard.set(user.id, forKey: Constants.Key.AccountId)
                    
                    
                }else{
                }
                
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func login(user: String, pass: String, onComplet : @escaping(Any)->Void){
        //{{host}}authentication/token/validate_with_login?api_key={{keyCode}}&username=andrehaas&password=haas527722&request_token=20b6b7cac2635102d8b124c26a3b0a5fb36a1ec0
        if let request_token = UserDefaults.standard.object(forKey: "Token")  {
            var url = "\(Constants.urlBase)authentication/token/validate_with_login?api_key=\(Constants.chaveAcesso)"
            self.token = request_token as! String
            url += "&username=\(user)&password=\(pass)&request_token=\(self.token!)"
            
            Alamofire.request(url).responseJSON(completionHandler: { (response) in
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    
                    if let retorno = json as? [String : Any]{
                        if let erro = retorno["status_message"] as? String {
                            print(erro)
                            onComplet(erro)
                        }else{
                            onComplet(true)
                        }
                    }
                    onComplet(true)
                    self.openSession()
                case .failure(let error):
                    print(error.localizedDescription)
                    onComplet(false)
                }
            })
            
        }
    }
    
    func openSession(){
        //https://api.themoviedb.org/3/authentication/session/new?api_key=8fb08dee4651c3d9b1536a69ea3f3d16&request_token=20b6b7cac2635102d8b124c26a3b0a5fb36a1ec0
        
        let url = "\(Constants.urlBase)authentication/session/new?api_key=\(Constants.chaveAcesso)&request_token=\(self.token!)"
        
        Alamofire.request(url).responseJSON { (response) in
            switch response.result {
                
            case .success(let json):
                print(json)
                do {
                    let session = try JSONDecoder().decode(SessionModel.self, from: response.data!)
                    UserDefaults.standard.set(session.session_id, forKey: "SessionID")
                    
                }catch{
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
