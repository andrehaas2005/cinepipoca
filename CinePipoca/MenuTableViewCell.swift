//
//  MenuTableViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 19/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var iconMenu: UIImageView!
    @IBOutlet weak var labelMenu: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
