//
//  AtorModel.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation


struct CapaAtor : Decodable {
    let id : Int
    let cast : [Ator]
    let crew : [Crew]
}

struct Ator : Decodable {
    let cast_id : Int
    let character : String
    let credit_id : String
    let gender : Int = 0
    let id : Int = 0
    let name : String
    let order : Int = 0
    let profile_path : String?
    
}

struct Crew : Decodable {
   let credit_id : String
   let  department : String
    let gender : Int
    let id : Int
    let job : String
    let name : String
    let profile_path : String?
}
