//
//  SerieTableViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 26/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class SerieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viPoster: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbResumo: UILabel!
    let  urlImage =  Constants.urlImage

    @IBOutlet weak var lbNota: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepareSerie(_ serie: Serie){
        lbTitulo.text = serie.name
        lbResumo.text = serie.overview
        //lbNota.text = String(Int(serie.vote_average))
        var totalVotos : String = ""
        for _ in 0...Int(serie.vote_average){
            totalVotos  += "⭐️ "
        }
        self.lbNota.text = totalVotos + " (\(serie.vote_average) )"
        viPoster.kf.indicatorType = .activity
        if let poster = serie.poster_path {
        if let url = URL(string: urlImage + poster){
            viPoster.kf.setImage(with: url)
        }else{
            viPoster.image = UIImage(named: "semImages")
        }
        }else{
            viPoster.image = UIImage(named: "semImages")
        }
    }

}
