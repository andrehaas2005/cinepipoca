//
//  SeriesView.swift
//  CinePipoca
//
//  Created by André Haas on 02/06/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

protocol SeriesView {
    func alert(type: TypeResponse)
}
