//
//  DateHelper.swift
//  CinePipoca
//
//  Created by André Haas on 08/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

class DateHelper {
    
    class func FormatarStringData(dataString: String) -> String{
        //yyyy-MM-dd
        
        let dataEntrada = DateFormatter()
        dataEntrada.dateFormat = "yyyy-MM-dd"
        if let dtentrada = dataEntrada.date(from: dataString){
        
        let dataSaida = DateFormatter()
        dataSaida.dateFormat = "dd/MM/yyyy"
        
        let dtSaidaDate = dataSaida.string(from: dtentrada)
        
        return dtSaidaDate
        }
        return dataString
        
    }
    
    //2017-12-14T00:00:00.000Z
    class func FormatarStringDataZ(dataString: String) -> String {
        let dataEntrada = DateFormatter()
        dataEntrada.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
        if let dtEntrada = dataEntrada.date(from: dataString){
          let dataSaida = DateFormatter()
            dataSaida.dateFormat = "dd/MM/yyyy"
            
            let dtSaidaDate = dataSaida.string(from: dtEntrada)
            return dtSaidaDate
        }
        return dataString
    }
    
    class func FormatarDataToStringHora(date: Date)-> String{
        let dataFormato = DateFormatter()
        dataFormato.dateFormat = "HH:mm"
        
        dataFormato.timeZone = TimeZone(abbreviation: "UTC")
        dataFormato.locale = Locale(identifier: "pt_BR")
       return dataFormato.string(from: date)
    }
    
    class func FormatarStringToData(dataString: String) -> Date {
        
        let dataFormato = DateFormatter()
        dataFormato.dateFormat = "dd/MM/yyyy HH:mm"
        
        return dataFormato.date(from: dataString)!
        
    }
    
    class func FormatarDateToString(data: Date) -> String{
        //yyyy-MM-dd
        
        let dataEntrada = DateFormatter()
        dataEntrada.dateFormat = "HH:mm"
       let dtSaidaDate = dataEntrada.string(from: data)
        
        return dtSaidaDate
        
    }
    
    class func FormatarStringToDate(dataString: String) -> Date{
        //yyyy-MM-dd
        
        let dataEntrada = DateFormatter()
        dataEntrada.dateFormat = "yyyy-MM-dd"
         let dtSaidaDate = dataEntrada.date(from: dataString)
        
        return dtSaidaDate!
        
    }
    
    class func ReturnaMaiorData(dataString: String) ->String{
        let dataEntrada = DateFormatter()
        dataEntrada.dateFormat = "yyyy-MM-dd"
        let dtentrada = dataEntrada.date(from: dataString)
        
        let dataSaida = DateFormatter()
        dataSaida.dateFormat = "dd/MM/yyyy"
        
        var dtSaidaDate = ""
        
        
        if dtentrada! >= Date() {
          dtSaidaDate =  dataSaida.string(from: dtentrada!)
        }else{
            dtSaidaDate =  dataSaida.string(from: Date())
        }
        
        return dtSaidaDate
    }
    
    
    
}
