//
//  TypeResponse.swift
//  CinePipoca
//
//  Created by André Haas on 31/05/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//


enum TypeResponse {
    case Sucesso(Any)
    case SemInternet
    case Error(Any)
}
