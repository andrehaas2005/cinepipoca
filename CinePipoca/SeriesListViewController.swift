//
//  SeriesTableViewController.swift
//  CinePipoca
//
//  Created by André Haas on 26/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//
// Tela que lista as series


import UIKit

enum tipoListagem : String {
    case Popular = "Séries Populares"
    case NoAr = "Série no Ar"
    case porBusca = "Busca por : "
}




class SeriesListViewController: GenericViewController {
    
    var series : [Serie] = []
    var pageTotal = 1
    var pageAtual = 1
    var loading = false
    var total = 0
    var showSearch = false
    var searcSeries = false
    var seriesNoAr = false
    var isMenuOpen = false
    let presenterSerie = SeriesPresenter(service: SeriesManager())
    var tipo : tipoListagem!
    var searchString = ""
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintTable: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintTable.constant = -56.0
        self.presenterSerie.attachView(self)
        self.title = "Séries Populares"
        super.showHud()
        self.tipo = tipoListagem.Popular
        loadSeries()
        search.delegate = self
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showSearch = false
        search.isHidden = true
        constraintTable.constant = -56.0
    }
    
    @IBAction func menuOpen(_ sender: Any) {
        if isMenuOpen {
            super.menuClose()
        }else{
            super.menuOpen()
        }
    }
    
    @IBAction func buttonActionOpetions(_ sender: Any) {
        
        let alert = UIAlertController(title: "Opções", message: nil, preferredStyle: .actionSheet)
        
        let listNoAr = UIAlertAction(title: tipoListagem.NoAr.rawValue, style: .default) { (action) in
            self.pageAtual = 1
            self.tipo = tipoListagem.NoAr
            self.series = []
            self.total = 0
            self.seriesNoAr = true
            self.title = self.tipo.rawValue
            self.showHud()
            self.loadSeriesNoAr()
        }
        let listSerie = UIAlertAction(title: tipoListagem.Popular.rawValue, style: .default) { (action) in
            self.pageAtual = 1
            self.tipo = tipoListagem.Popular
            self.series = []
            self.total = 0
            self.title = self.tipo.rawValue
            self.seriesNoAr = false
            self.showHud()
            self.loadSeries()
        }
        
        let actionCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alert.addAction(listNoAr)
        alert.addAction(listSerie)
        alert.addAction(actionCancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetailSerie"{
            let vc = segue.destination as! SerieViewController
            let serie = sender as! Serie
            vc.id_serie = serie.id
            
        }
    }
    
    
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        search.isHidden = showSearch
        showSearch = !showSearch
        if showSearch {
            constraintTable.constant = 0.0
        }else{
            constraintTable.constant = -56.0
        }
       
    }
    
    
}
extension SeriesListViewController : UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 193.0
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return series.count
    }

  
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SerieTableViewCell

        let serie = series[indexPath.row]
        cell.prepareSerie(serie)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let serie = self.series[indexPath.row]
        self.tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueDetailSerie", sender: serie)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if( indexPath.row == (series.count) - 10 ) && (!loading) && (series.count != total) && (pageAtual != pageTotal) {
            pageAtual += 1
            
            switch tipo as tipoListagem  {
                
            case .Popular:
                loadSeries()
            case .NoAr:
                loadSeriesNoAr()
            case .porBusca:
                searchSerie(strSearch: self.searchString)
            }
        }
    }
 
}
extension SeriesListViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let query = searchBar.text {
            self.showHud()
            self.series = []
            self.pageAtual = 1
            self.tipo = tipoListagem.porBusca
            self.title = self.tipo.rawValue + query
            self.searchString = query
            self.searchSerie(strSearch: query)
        }

    }
}

extension SeriesListViewController : SeriesView {
    func alert(type: TypeResponse) {
        super.alertGeneric(typeResponse: type, view: self)
    }
    
    func loadSeriesNoAr(){
        presenterSerie.getSeriesNoAr(page: self.pageAtual) { (retorno) in
            self.series += retorno.results
            self.total = retorno.total_results
            self.pageTotal = retorno.total_pages
            self.loading = false
            self.tableView.reloadData()
            self.hideHud()
        }
    }
    
    func loadSeries(){
        presenterSerie.getSeries(page: self.pageAtual) { (retorno) in
            self.series += retorno.results
            self.total = retorno.total_results
            self.pageTotal = retorno.total_pages
            self.loading = false
          
            self.tableView.reloadData()
            self.hideHud()
        }
    }
    
    func searchSerie(strSearch: String){
        
        searcSeries = true
        loading = true
        presenterSerie.searchSeries(page: pageAtual, query: strSearch) { (retorno) in
            self.series += retorno.results
            self.total = retorno.total_results
            self.pageTotal = retorno.total_pages
            self.tableView.reloadData()
            self.loading = false
            self.hideHud()
        }

    }
}
