//
//  SeriesModel.swift
//  CinePipoca
//
//  Created by André Haas on 26/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

struct CapaSerie : Decodable{
    let page : Int
    let total_results : Int
    let total_pages : Int
    let results : [Serie]
}

struct Serie : Decodable {
    let original_name :String
    let genre_ids :[Int]
    let name:String
    let popularity : Double
    let origin_country :[String]
    let vote_count :Int
    let first_air_date: String
    let backdrop_path :String?
    let original_language: String?
    let id: Int
    let vote_average: Double
    let overview : String?
    let poster_path:String?
}
