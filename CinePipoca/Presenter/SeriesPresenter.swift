//
//  SeriesPresenter.swift
//  CinePipoca
//
//  Created by André Haas on 02/06/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

class SeriesPresenter{
    
    var serieView : SeriesView!
    var seriesService : SeriesManager!
    
    init(service : SeriesManager) {
        self.seriesService = service
    }
    
    func attachView(_ view: SeriesView){
        serieView = view
    }
    
    func getSeriesNoAr(page: Int = 1, onComplet : @escaping(CapaSerie)->Void){
        
        seriesService.getSeriesNoAr(page: page) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let capa = retorno as? CapaSerie {
                        onComplet(capa)
                    }
                case .SemInternet:
                    self.serieView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.serieView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
    
     func getSeries(page: Int = 1,onComplet : @escaping(CapaSerie)->Void){
        seriesService.getSeries(page: page) { (response) in
            
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let capa = retorno as? CapaSerie {
                        onComplet(capa)
                    }
                case .SemInternet:
                    self.serieView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.serieView.alert(type: TypeResponse.Error(error))
                }
            }
            
        }
    }
    
    
    func searchSeries(page: Int = 1, query: String,onComplet : @escaping(CapaSerie)->Void){
        seriesService.searchSeries(page: page, query: query) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let capa = retorno as? CapaSerie {
                        onComplet(capa)
                    }
                case .SemInternet:
                    self.serieView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.serieView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
}
