//
//  CinePresenter.swift
//  CinePipoca
//
//  Created by André Haas on 31/05/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation

class CinePresenter{
    
    var cineService : CineMananger!
    var cineView : CineView!
    
    init(cineService : CineMananger) {
        self.cineService = cineService
    }
    func attach(_ view : CineView){
        self.cineView = view
    }
    
    func loadMovieNowPlaying(page: Int = 1, onComplet : @escaping(Capa)->Void){
        cineService.loadMovieNowPlaying(page: page) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let movies = retorno as? Capa {
                        onComplet(movies)
                    }else{
                        self.cineView.alert(type: TypeResponse.Error("Erro"))
                    }
                case .SemInternet:
                    self.cineView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.cineView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
    
    func loadMovieFavoritos(page: Int = 1, onComplet : @escaping(Capa)->Void){
        cineService.loadMovieFavoritos(page: page) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let movies = retorno as? Capa {
                        onComplet(movies)
                    }else{
                        self.cineView.alert(type: TypeResponse.Error("Erro"))
                    }
                case .SemInternet:
                    self.cineView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.cineView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
    
    func listaInteresse(page: Int = 1,onComplet : @escaping(Capa)-> Void ){
        cineService.listaInteresse(page: page) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let movies = retorno as? Capa {
                        onComplet(movies)
                    }else{
                        self.cineView.alert(type: TypeResponse.Error("Erro"))
                    }
                case .SemInternet:
                    self.cineView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.cineView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
    
    func loadMoviePopular(page: Int = 0, onComplet : @escaping(Capa)->Void){
        cineService.loadMoviePopular(page: page) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let movies = retorno as? Capa {
                        onComplet(movies)
                    }else{
                        self.cineView.alert(type: TypeResponse.Error("Erro"))
                    }
                case .SemInternet:
                    self.cineView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.cineView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
    
    func searchMovie(page: Int = 1 , query: String, onComplet : @escaping(Capa)->Void){
        cineService.searchMovie(page: page, query: query) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let movies = retorno as? Capa {
                        onComplet(movies)
                    }else{
                        self.cineView.alert(type: TypeResponse.Error("Erro"))
                    }
                case .SemInternet:
                    self.cineView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.cineView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
    func listProximosLancamentos(page : Int = 1, onComplet : @escaping(Capa)->Void){
        cineService.listProximosLancamentos(page: page) { (response) in
            DispatchQueue.main.async {
                switch response {
                    
                case .Sucesso(let retorno):
                    if let movies = retorno as? Capa {
                        onComplet(movies)
                    }else{
                        self.cineView.alert(type: TypeResponse.Error("Erro"))
                    }
                case .SemInternet:
                    self.cineView.alert(type: TypeResponse.SemInternet)
                case .Error(let error):
                    self.cineView.alert(type: TypeResponse.Error(error))
                }
            }
        }
    }
}
