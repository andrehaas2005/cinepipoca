//
//  SerieViewController.swift
//  CinePipoca
//
//  Created by André Haas on 29/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import Kingfisher


class SerieViewController: GenericViewController {
    
    var id_serie : Int!
    var detail : DetailSerieModel!
    var episodios : [Season] = []
    var numeroEpisodios = 0
    let  urlImage =  Constants.urlImage
    var seriesManager = SeriesManager.share
    
    @IBOutlet weak var imPoster: UIImageView!
    @IBOutlet weak var lbResumo: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var viewFull: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDetalSerie(id_serie)
        
    }

    private func setupTableView(){
        
        tableView.register(UINib(nibName: "SesseosTableViewCell", bundle: nil), forCellReuseIdentifier: "SesseosTableViewCellID")
    }
    
    func loadDetalSerie(_ id : Int){
        
        self.showHud()
        seriesManager.getSerieById(serie_id: id_serie) { (retorno) in
            if let detailRetorno = retorno {
                self.detail = detailRetorno
                self.title = detailRetorno.name
                self.imPoster.kf.indicatorType = .activity
                
                if let url = detailRetorno.poster_path {
                    let urlString = URL(string: Constants.urlImage + url)
                    self.imPoster.kf.setImage(with: urlString!)
                    do{
                        let imagData = try Data(contentsOf: urlString!)
                        if let imag =  UIImage(data: imagData){
                    
                        self.getCores(imag)
                        }
                    self.viewFull.backgroundColor = self.colorPrimary
                    self.lbResumo.textColor = self.colorDetail
                    }catch{
                        print(error)
                    }
                }else{
                    self.imPoster.image = UIImage(named: "semImages")
                }
                self.lbResumo.text = detailRetorno.overview
                self.numeroEpisodios = self.detail.seasons.count
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
                self.hideHud()
                //self.loadEpisodios(detailRetorno)
            }
        }
    }
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetailsSession"{
            
            let session = sender as! Seasons
            let vc = segue.destination as! SessionDetailsViewController
            vc.serieId = id_serie
            vc.seasonNumber = session.season_number
            vc.nomeSerie = self.detail.name
            vc.colorBackGroud = self.colorBackGroud
            
        }
    }

}

extension SerieViewController : UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Temporadas"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return self.numeroEpisodios
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath)
        let session = self.detail.seasons[indexPath.row]
     cell.textLabel?.text = "\(session.season_number) Temporada - \(session.episode_count) episodios"
 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let session = self.detail.seasons[indexPath.row]
        performSegue(withIdentifier: "segueDetailsSession", sender: session)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
