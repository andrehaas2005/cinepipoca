//
//  AtoresCollectionViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 24/01/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import Kingfisher

class AtoresCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ivAtores: UIImageView!
    @IBOutlet weak var lbNomeAtor: UILabel!
    @IBOutlet weak var lbPersonagem: UILabel!
    
    
    func prepareAtor(_ ator: Ator){
        lbPersonagem.text = ator.character
        lbNomeAtor.text = ator.name
        ivAtores.kf.indicatorType = .activity
        if let profileAtor = ator.profile_path {
            let urlString = Constants.urlImage + profileAtor
            if let url = URL(string: urlString) {
                ivAtores.kf.indicatorType = .activity
                ivAtores.kf.setImage(with: url)
            }else{
                self.ivAtores.image = UIImage(named: "semImages")
            }
        }else{
            self.ivAtores.image = UIImage(named: "semImages")
        }
        
    }
    
}
