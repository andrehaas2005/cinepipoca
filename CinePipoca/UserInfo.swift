//
//  UserInfo.swift
//  CinePipoca
//
//  Created by André Haas on 19/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import Foundation
import UIKit

class UserInfo : Decodable {
    var avatar : Gravatar!
    var id : Int!
    var iso_639_1 : String!
    var iso_3166_1 : String!
    var name : String!
    var include_adult : Bool!
    var username : String!
    
    init() {
        
    }

}
class Gravatar : Decodable {
    
    var gravatar : Hash
    init(gravatar : Hash) {
        self.gravatar = gravatar
    }
  
}
class Hash : Decodable {
    
    var hash : String!
    
    
}
