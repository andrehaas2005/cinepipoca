//
//  CustomLoadView.swift
//  CinePipoca
//
//  Created by André Haas on 30/03/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit

class CustomLoadView: UIView {

    @IBOutlet weak var filme: UIImageView!
    @IBOutlet weak var rolo: UIImageView!
    @IBOutlet var topLevelView: UIView!
    @IBOutlet weak var backgroupView: UIView!
    @IBOutlet weak var messageWindow: UIView!
    
    
    var view: UIView!
    
    var parent: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    /**
     Initialiser method
     
     @parameter aDecoder: aDecoder
     */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    
    
    func displayView() {
        
        UIApplication.shared.keyWindow?.addSubview(topLevelView)
        
       // parent?.addSubview(topLevelView)
        
        topLevelView.layoutIfNeeded()
        messageWindow.layoutIfNeeded()
        rolo.rotate360Degrees(completionDelegate: self)
        
        //self.customIndicator.rotate360Degrees(completionDelegate: self)
    }
    
    private func setupView(){
        view = loadViewFromXibFile()
        
        parent = UIApplication.shared.keyWindow?.subviews.last
        
        if parent != nil {
            
            topLevelView.frame = parent!.frame
        }
    }
    
    func hideView() {
        topLevelView.removeFromSuperview()
    }
    
    private func loadViewFromXibFile()-> UIView {
        let bundle = Bundle(for: type(of: self)) //Bundle(forClass: type(of: self))
        let nib = UINib(nibName: "CustomLoadView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

}
