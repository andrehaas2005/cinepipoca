//
//  TrailerCollectionViewCell.swift
//  CinePipoca
//
//  Created by André Haas on 11/04/2018.
//  Copyright © 2018 André Haas. All rights reserved.
//

import UIKit
import WebKit

class TrailerCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var labelNome: UILabel!
    @IBOutlet weak var viewParaImagem: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
//390 X 218
